# Overview

This page is dedicated to the process of [auto-deploy](auto-deploy.md).

## Temporarily stopping automated deployments

In case of an issue that could cause a severe disruption of GitLab.com and the related environments, it might be necessary to stop automatic deployments from
running.

In case of such an event:

* Issue describing P1 and S1 needs to be created.
* In the `#releases` channel, run:
  ```
  /chatops run auto_deploy pause
  ```

Stopping the `Auto-deploy tagging` job will effectively prevent the tooling from creating new packages and triggering automated deploys to the staging environment. 
Ensuring that release managers are informed will ensure that manual part of deployment (progression to production environments) is not executed.

Once the issue is resolved, release managers need to make a decision on case per case basis on what is the best way to progress further towards enabling the automated deployments again.
