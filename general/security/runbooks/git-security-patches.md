# How to handle Git security patches?

This runbook describes how to:
- Deploy a Git vulnerability fix that is under embargo (cannot be made public) to GitLab.com.

The embargo dates are usually decided by the upstream Git team. A Gitaly team member usually interacts with the Git team
and keeps us informed.

Since the Git source has to be included with a self-managed release (due to the license), we cannot release a self-managed
package with the fix until the embargo is lifted.

## Deploy the Git fix to GitLab.com

Before the embargo is lifted, we usually need to deploy the fix to GitLab.com. Since the embargo has not been lifted yet,
the deploy needs to happen without making the fix public.

1. Gitaly creates MRs in the [Git Security Mirror]. Usually, one MR for
the `main` branch, and one MR per Git version (ex: `gitlab-git-v2.38` branch) that needs to be patched for GitLab.com.

2. Once the branch MR (ex: `gitlab-git-v2.38` branch) has been merged, the Gitaly team will create a tag (ex: `v2.38.gl3`).

3. On [Omnibus dev CI/CD variables], set the `GITALY_GIT_VERSION_x_xx` environment variable to the Git tag (e.g
`v2.38.3.gl3`) that needs to be deployed. If the version being deployed is `v2.38.x`,
the variable should be named `GITALY_GIT_VERSION_2_38`.

   Gitaly uses a variable to override the minor version that is bundled with Omnibus. For example, if v2.38 is the
version that is currently deployed to GitLab.com, it is necessary to override the v2.38 minor version that is deployed,
to deploy the patched version to GitLab.com. To do this, Omnibus needs to set the `GIT_VERSION_2_38` environment variable
to v2.38.3.gl3 when `make`ing Gitaly. Omnibus sets the `GIT_VERSION_2_38` variable to the value of `GITALY_GIT_VERSION_2_38`.
This only overrides the Git version for the auto-deploy package, not for any self-managed packages.

4. Set the `GITALY_GIT_REPO_URL` variable in [Omnibus dev CI/CD variables].
You can copy the value of `GIT_REPO_URL` in <https://gitlab.com/gitlab-org/security/gitlab/-/settings/ci_cd>.

   The value should be `https://username:token@gitlab.com/gitlab-org/security/git.git`. `username` can be any string.
`token` should be a project token from [Git Security Mirror] with the `read_repository` permission. If you do create a
new token, make sure to follow the [token management process](../../../runbooks/token-management.md) to request and
document a new token ([example](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/2685)).

5. Increment the value of `AUTO_DEPLOY_CACHE_REVISION` in [Omnibus dev CI/CD variables]
in order to bust the auto deploy package cache.

   For example, if the value is `1.0`, you can set it to `2.0`.

6. Start a deployment pipeline by executing `auto_deploy:prepare` and then `auto_deploy:tag` in
<https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules>. As long as the above variables are present in
<https://dev.gitlab.org/gitlab/omnibus-gitlab/-/settings/ci_cd>, every auto deploy package will contain the given Git
version. Since we updated the cache revision number, the Omnibus runner cache will start fresh and the first Omnibus
packager pipeline will take longer than usual, by about 60 minutes.

7. Once deployment is complete, ask the SRE-on-call to verify that the deployed git versions are correct. On the Gitaly
VM, execute the following:
   ```shell
   root@c69ddea90c42:/# /opt/gitlab/embedded/bin/gitaly-git-v2.38 --version
   git version 2.38.3.gl3
   ```

[Git Security Mirror]: https://gitlab.com/gitlab-org/security/git
[Omnibus dev CI/CD variables]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/settings/ci_cd
